for file in *.svg; do
	for size in "mdpi 32" "hdpi 48" "xhdpi 64"; do
		type=$(echo $size | cut -f 1 -d " ")
		dim=$(echo $size | cut -f 2 -d " ")
		rsvg -w $dim -h $dim $file ../res/drawable-$type/$(basename -s .svg $file).png
	done
done
