package com.example.ciclopi;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;

import com.example.ciclopi.core.Configuration;
import com.example.ciclopi.core.StationsUpdateService;
import com.example.ciclopi.preferences.CiclopiPreferencesActivity;

public class MainActivity extends FragmentActivity implements View.OnClickListener {
    private Button stationListButton;
    private Button mapButton;
    private Button informationButton;
    private Button settingsButton;
    private Configuration configuration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Intent intent = new Intent(this, StationsUpdateService.class);
        startService(intent);

        configuration = Configuration.forContext(this);

        stationListButton = (Button) findViewById(R.id.button_station_list);
        mapButton = (Button) findViewById(R.id.button_map);
        informationButton = (Button) findViewById(R.id.button_information);
        settingsButton = (Button) findViewById(R.id.button_settings);


        stationListButton.setOnClickListener(this);
        mapButton.setOnClickListener(this);
        informationButton.setOnClickListener(this);
        settingsButton.setOnClickListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();

        configuration.onStart(this);
    }

    @Override
    protected void onStop() {
        configuration.onStop(this);

        super.onStop();
    }

    @Override
    public void onClick(final View v) {
        Intent stationsIntent = new Intent(this, StationsActivity.class);

        if (v == stationListButton) {
            startActivity(stationsIntent);
        }
        if (v == mapButton) {
            stationsIntent.putExtra(StationsActivity.MAP_EXTRA, true);
            startActivity(stationsIntent);
        }
        if (v == informationButton) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.ciclopi.eu/")));
        }
        if (v == settingsButton) {
            startActivity(new Intent(this, CiclopiPreferencesActivity.class));
        }
    }
}
