package com.example.ciclopi.preferences;

public class PreferenceKeys {
    public static final String BACKGROUND_REFRESH_PREFERENCE = "background_refresh_preference";
    public static final String AUTO_REFRESH_PREFERENCE = "auto_refresh_preference";
    public static final String REFRESH_INTERVAL_PREFERENCE = "refresh_interval_preference";
    public static final String BACKGROUND_REFRESH_INTERVAL_PREFERENCE = "background_refresh_interval_preference";

    private PreferenceKeys() {}
}
