package com.example.ciclopi.preferences;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceActivity;

import com.example.ciclopi.R;
import com.example.ciclopi.core.Configuration;
import com.example.ciclopi.core.StationsUpdateService;

public class CiclopiPreferencesActivity extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
    private Configuration configuration;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.preferences);

        configuration = Configuration.forContext(this);

        configuration.getPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected void onDestroy() {
        configuration.getPreferences().unregisterOnSharedPreferenceChangeListener(this);

        super.onDestroy();
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        // make sure service is active to start alarms
        startService(new Intent(this, StationsUpdateService.class));
    }
}