package com.example.ciclopi.updateheader;

import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.ciclopi.R;

public class UpdateHeaderViewHolder {
    public final TextView updatedDateTextView;
    public final ProgressBar progressBar;

    private UpdateHeaderViewHolder(View rootView) {
        updatedDateTextView = (TextView) rootView.findViewById(R.id.updated_date_text_view);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progress_bar);
    }

    public static UpdateHeaderViewHolder forView(View rootView) {
        return new UpdateHeaderViewHolder(rootView);
    }
}
