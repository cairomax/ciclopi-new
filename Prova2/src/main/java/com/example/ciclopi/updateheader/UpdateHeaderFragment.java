package com.example.ciclopi.updateheader;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.ciclopi.R;
import com.example.ciclopi.core.Configuration;
import com.example.ciclopi.core.LastUpdateFormatter;
import com.example.ciclopi.model.Model;
import com.example.ciclopi.model.ServerClock;
import com.example.ciclopi.model.StationsInfo;

public class UpdateHeaderFragment extends Fragment implements StationsInfo.OnStationListUpdateListener, ServerClock.OnTickListener {
    public static final int CHECK_TICKS_INTERVAL = 5000;
    private UpdateHeaderViewHolder views;
    private StationsInfo stationsInfo;
    private Runnable updateAction = new Runnable() {
        @Override
        public void run() {
            update();
        }
    };
    private ServerClock serverClock;
    private LastUpdateFormatter lastUpdateFormatter;
    private Handler serverClockTimeHandler;

    private Runnable checkTicksAction = new Runnable() {
        @Override
        public void run() {
            serverClock.checkTicks();

            if (isVisible()) {
                sheduleCheckTicksAction();
            }
        }
    };

    @Override
    public void onAttach(final Activity activity) {
        super.onAttach(activity);

        Configuration configuration = Configuration.forContext(activity);
        Model model = configuration.getModel();

        serverClockTimeHandler = new Handler();

        stationsInfo = model.getStationsInfo();
        serverClock = model.getServerClock();
        lastUpdateFormatter = configuration.getLastUpdateFormatter();
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_update_header, null);
    }

    @Override
    public void onActivityCreated(final Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        views = UpdateHeaderViewHolder.forView(getView());
    }

    @Override
    public void onStart() {
        super.onStart();

        stationsInfo.registerOnStationListUpdatedListener(this);
        serverClock.addOnTickListener(this);

        onStationListUpdated();

        sheduleCheckTicksAction();
    }

    private boolean sheduleCheckTicksAction() {
        return serverClockTimeHandler.postDelayed(checkTicksAction, CHECK_TICKS_INTERVAL);
    }

    @Override
    public void onStop() {
        stationsInfo.unregisterOnStationListUpdatedListener(this);
        serverClock.removeOnTickListener(this);

        super.onStop();
    }

    private void update() {
        if (stationsInfo.getUpdatedTimeIfLoaded().isPresent()) {
            long updatedTime = stationsInfo.getUpdatedTimeIfLoaded().get();
            CharSequence formatted = lastUpdateFormatter.format(updatedTime, serverClock.getCurrentTime(), serverClock.isAccurate());
            views.updatedDateTextView.setText(formatted);
        } else {
            views.updatedDateTextView.setText(getResources().getString(R.string.loading));
        }

        views.progressBar.setVisibility(stationsInfo.isUpdating() ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public void onStationListUpdateStarted() {
        updateOnUiThread();
    }

    @Override
    public void onStationListUpdateFailure() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getActivity(), getResources().getString(R.string.update_failed), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onStationListUpdateFinished() {
        updateOnUiThread();
    }

    @Override
    public void onStationListUpdated() {
        updateOnUiThread();
    }

    private void updateOnUiThread() {
        getActivity().runOnUiThread(updateAction);
    }

    @Override
    public void onServerClockTick() {
        updateOnUiThread();
    }
}
