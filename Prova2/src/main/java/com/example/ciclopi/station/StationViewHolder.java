package com.example.ciclopi.station;

import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TextView;

import com.example.ciclopi.R;

public class StationViewHolder {
    public final TextView nameTextView;
    public final StationCompassView compassView;
    public final TextView distanceTextView;
    public final TextView bikeSlotsCounterView;
    public final TextView vacantSlotsCounterView;
    public final TextView unknownSlotsCounterView;
    public final TableLayout slotsTableLayout;
    public final Button viewOnMapButton;
    public final Button directionsButton;
    public final Button navigateButton;

    private StationViewHolder(View rootView) {
        nameTextView = (TextView) rootView.findViewById(R.id.name_text);
        compassView = (StationCompassView) rootView.findViewById(R.id.station_compass);
        distanceTextView = (TextView) rootView.findViewById(R.id.distance_text);
        bikeSlotsCounterView = (TextView) rootView.findViewById(R.id.bike_slots_counter);
        vacantSlotsCounterView = (TextView) rootView.findViewById(R.id.vacant_slots_counter);
        unknownSlotsCounterView = (TextView) rootView.findViewById(R.id.unknown_slots_counter);
        slotsTableLayout = (TableLayout) rootView.findViewById(R.id.slots_table_layout);
        viewOnMapButton = (Button) rootView.findViewById(R.id.view_on_map_button);
        directionsButton = (Button) rootView.findViewById(R.id.directions_button);
        navigateButton = (Button) rootView.findViewById(R.id.navigate_button);
    }

    public static StationViewHolder forView(View rootView) {
        return new StationViewHolder(rootView);
    }
}
