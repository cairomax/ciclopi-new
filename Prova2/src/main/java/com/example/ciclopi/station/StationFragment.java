package com.example.ciclopi.station;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ciclopi.R;
import com.example.ciclopi.StationsActivity;
import com.example.ciclopi.core.Configuration;
import com.example.ciclopi.core.DirectionsHelper;
import com.example.ciclopi.core.SlotsTableHelper;
import com.example.ciclopi.core.StationLocationHelper;
import com.example.ciclopi.core.Watermark;
import com.example.ciclopi.core.WatermarkColors;
import com.example.ciclopi.map.CiclopiMapFragment;
import com.example.ciclopi.model.Model;
import com.example.ciclopi.model.MyLocationInfo;
import com.example.ciclopi.model.Station;
import com.example.ciclopi.model.StationsInfo;

import java.util.Collections;

public class StationFragment extends Fragment implements StationsInfo.OnStationListUpdateListener, MyLocationInfo.MyLocationUpdateListener, View.OnClickListener {
    public static final String STATION_ID_ARGUMENT = "station_id";
    private final Runnable updateAction = new Runnable() {
        @Override
        public void run() {
            update();
        }
    };

    private Configuration configuration;
    private String stationId;

    private StationViewHolder views;
    private StationLocationHelper stationLocationHelper;
    private Model model;
    private StationsInfo stationsInfo;
    private MyLocationInfo myLocationInfo;
    private SlotsTableHelper slotsTableHelper;
    private WatermarkColors watermarkColors;
    private DirectionsHelper directionsHelper;
    private Station station;

    @Override
    public void onAttach(final Activity activity) {
        super.onAttach(activity);

        configuration = Configuration.forContext(activity);
        model = configuration.getModel();
        stationsInfo = model.getStationsInfo();
        myLocationInfo = model.getMyLocationInfo();
        stationLocationHelper = configuration.getStationLocationHelper();
        slotsTableHelper = configuration.getSlotsTableHelper();
        watermarkColors = configuration.getWatermarkColors();
        directionsHelper = configuration.getDirectionsHelper();

        stationId = getArguments().getString(STATION_ID_ARGUMENT);

        station = stationsInfo.getStationsIfLoaded().or(Collections.EMPTY_MAP).get(stationId);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_station, null);
    }

    @Override
    public void onActivityCreated(final Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        views = StationViewHolder.forView(getView());

        views.viewOnMapButton.setOnClickListener(this);
        views.directionsButton.setOnClickListener(this);
        views.navigateButton.setOnClickListener(this);
    }

    @Override
    public void onStart() {
        super.onStart();

        stationsInfo.registerOnStationListUpdatedListener(this);
        myLocationInfo.registerMyLocationUpdateListener(this);

        onStationListUpdated();
    }

    @Override
    public void onStop() {
        stationsInfo.unregisterOnStationListUpdatedListener(this);
        myLocationInfo.unregisterMyLocationUpdateListener(this);

        super.onStop();
    }

    @Override
    public void onStationListUpdateStarted() {

    }

    @Override
    public void onStationListUpdateFailure() {

    }

    @Override
    public void onStationListUpdateFinished() {

    }

    @Override
    public void onStationListUpdated() {
        updateOnUiThread();
    }

    @Override
    public void onMyLocationUpdated() {
        updateOnUiThread();
    }

    private void updateOnUiThread() {
        getActivity().runOnUiThread(updateAction);
    }

    private void update() {
        if (station == null) {
            Log.e("Ciclopi", "Stations not loaded when showing StationFragment");
            return;
        }

        stationLocationHelper.showDistance(views.distanceTextView, myLocationInfo, station);
        stationLocationHelper.showBearing(views.compassView, myLocationInfo, station);

        slotsTableHelper.fillSlotsTable(station, views.slotsTableLayout, getLayoutInflater(null));

        views.nameTextView.setText(station.getName());

        showCount(views.bikeSlotsCounterView, station.bikesCount(), true);
        showCount(views.vacantSlotsCounterView, station.vacantSlotsCount(), true);
        showCount(views.unknownSlotsCounterView, station.unknownSlotsCount(), false);
    }

    private void showCount(TextView view, int count, boolean watermark) {
        view.setText(Integer.toString(count));
        if (watermark) {
            view.setTextColor(watermarkColors.colorFor(Watermark.fromCount(count)));
        }
    }

    @Override
    public void onClick(View view) {
        if (view == views.viewOnMapButton) {
            View mainContainer = getActivity().findViewById(R.id.fragment_main);
            boolean phoneLayout = mainContainer != null;

            if (phoneLayout) {
                Intent intent = new Intent(getActivity(), StationsActivity.class);
                intent.putExtra(StationsActivity.MAP_EXTRA, true);
                intent.putExtra(StationsActivity.STATION_ID_EXTRA, stationId);
                startActivity(intent);
            } else {
                FragmentManager fragmentManager = getFragmentManager();

                FragmentTransaction transaction = fragmentManager.beginTransaction();

                transaction.remove(this);

                Fragment oldMapFragment = fragmentManager.findFragmentByTag(StationsActivity.FRAGMENT_MAP_TAG);
                if (oldMapFragment != null) {
                    transaction.remove(oldMapFragment);
                }

                CiclopiMapFragment mapFragment = new CiclopiMapFragment();
                transaction.add(R.id.fragment_content, mapFragment, StationsActivity.FRAGMENT_MAP_TAG);
                transaction.addToBackStack(null);
                transaction.commit();

                mapFragment.onViewOnMap(station);
            }
        }

        if (view == views.directionsButton) {
            directionsHelper.showDirections(station);
        }

        if (view == views.navigateButton) {
            directionsHelper.startNavigation(station);
        }
    }
}
