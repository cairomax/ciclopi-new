package com.example.ciclopi.station;

import android.content.Context;
import android.graphics.*;
import android.util.AttributeSet;
import android.view.View;

public class StationCompassView extends View {
    public static final Paint ICON_PAINT = navigationIconPaint();
    private static final Path UPWARD_BEARING_ICON_PATH = upwardNavigationIconPath();
    private double bearing = 0.0;
    private boolean bearingKnown;

    public StationCompassView(final Context context) {
        super(context);
    }

    public StationCompassView(final Context context, final AttributeSet attrs) {
        super(context, attrs);
    }

    private static Path upwardNavigationIconPath() {
        final Path path = new Path();

        path.moveTo(0.0f, 0.0f);
        path.lineTo(+0.4f, 1.0f);
        path.lineTo(0.0f, 0.8f);
        path.lineTo(-0.4f, 1.0f);
        path.close();
        path.offset(0.0f, -0.5f);

        return path;
    }

    private static Paint navigationIconPaint() {
        final Paint paint = new Paint();

        paint.setStyle(Paint.Style.FILL);
        paint.setShader(new LinearGradient(0, -0.5f, 0, +0.5f, Color.LTGRAY, Color.GRAY, Shader.TileMode.CLAMP));
        paint.setAntiAlias(true);

        return paint;
    }

    @Override
    protected void onDraw(final Canvas canvas) {
        canvas.save();

        final int width = getWidth();
        final int height = getHeight();

        canvas.translate(width / 2f, height / 2f);

        final float scale = Math.min(width, height) * 0.8f;

        canvas.scale(scale, scale);

        if (bearingKnown) {
            canvas.rotate((float) bearing);
            drawBearingIcon(canvas);
        } else {
            drawUnknownBearingIcon(canvas);
        }

        canvas.restore();
    }

    private static void drawBearingIcon(Canvas canvas) {
        canvas.drawPath(UPWARD_BEARING_ICON_PATH, ICON_PAINT);
    }

    private static void drawUnknownBearingIcon(Canvas canvas) {
        canvas.drawCircle(0, 0, .2f, ICON_PAINT);
    }

    public void setBearing(final double rotation) {
        this.bearing = rotation;
        this.bearingKnown = true;
        invalidate();
    }

    public void setBearingUnknown() {
        this.bearingKnown = false;
    }
}
