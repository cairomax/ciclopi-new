package com.example.ciclopi.map;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ciclopi.R;
import com.example.ciclopi.StationsActivity;
import com.example.ciclopi.core.Configuration;
import com.example.ciclopi.core.Watermark;
import com.example.ciclopi.core.WatermarkColors;
import com.example.ciclopi.model.Model;
import com.example.ciclopi.model.Station;
import com.example.ciclopi.model.StationsInfo;
import com.example.ciclopi.station.StationFragment;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

public class CiclopiMapFragment extends Fragment implements StationsInfo.OnStationListUpdateListener, GoogleMap.OnInfoWindowClickListener, GoogleMap.OnMarkerClickListener, GoogleMap.InfoWindowAdapter, Observer {
    public static final LatLng MAP_CENTER = new LatLng(43.715907, 10.401943);
    private final Runnable updateStationListAction = new Runnable() {
        @Override
        public void run() {
            updateStations();
            updateViewOnMap();
        }
    };

    private Configuration configuration;
    private SupportMapFragment googleMapFragment;
    private ViewOnMapObservable viewOnMapObservable = new ViewOnMapObservable();
    private BiMap<Station, Marker> stationMarkerBiMap = HashBiMap.create();
    private WatermarkColors watermarkColors;

    @Override
    public void onAttach(final Activity activity) {
        super.onAttach(activity);
        configuration = Configuration.forContext(activity);
        watermarkColors = configuration.getWatermarkColors();
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_map, null);
    }

    @Override
    public void onActivityCreated(final Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        googleMapFragment = new SupportMapFragment();
        getChildFragmentManager().beginTransaction().replace(R.id.main_layout, googleMapFragment).commit();
    }

    @Override
    public void onStart() {
        super.onStart();

        final Model model = configuration.getModel();

        model.getStationsInfo().registerOnStationListUpdatedListener(this);
        viewOnMapObservable.addObserver(this);

        final GoogleMap map = googleMapFragment.getMap();

        if (map == null) {
            Log.e("Ciclopi", "GoogleMap not available. Not dying for debugging purpose.");
            return;
        }

        map.setOnMarkerClickListener(this);
        map.setOnInfoWindowClickListener(this);
        map.setInfoWindowAdapter(this);

        resetCamera();
        updateStations();
        updateViewOnMap();
    }

    private void updateStations() {
        final GoogleMap googleMap = googleMapFragment.getMap();
        final Model model = configuration.getModel();

        if (googleMap == null) {
            return;
        }

        googleMap.setMyLocationEnabled(true);

        googleMap.clear();
        stationMarkerBiMap.clear();

        if (model.getStationsInfo().getStationsIfLoaded().isPresent()) {
            updateMarkers();
        }
    }

    private void resetCamera() {
        final GoogleMap googleMap = googleMapFragment.getMap();
        final Model model = configuration.getModel();
        final Collection<Station> stations = model.getStationsInfo().getStationsIfLoaded().or(Collections.EMPTY_MAP).values();

        final LatLngBounds.Builder boundsBuilder = LatLngBounds.builder();
        boundsBuilder.include(MAP_CENTER);
        for (Station station : stations) {
            boundsBuilder.include(station.getLocation());
        }

        final LatLngBounds bounds = boundsBuilder.build();

        final int width = 300;
        final int height = 300;
        final int padding = 50;

        final CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);

        googleMap.moveCamera(cameraUpdate);
    }

    private void updateMarkers() {
        final GoogleMap googleMap = googleMapFragment.getMap();
        final Model model = configuration.getModel();
        final Map<String, Station> stations = model.getStationsInfo().getStationsIfLoaded().get();

        for (Station station : stations.values()) {
            final Marker marker = googleMap.addMarker(configureStationMarker(station));
            stationMarkerBiMap.put(station, marker);
        }
    }

    private MarkerOptions configureStationMarker(final Station station) {
        final MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(station.getLocation());
        markerOptions.title(station.getName());
        markerOptions.snippet(String.format("%d bikes, %d available spaces", station.bikesCount(), station.vacantSlotsCount()));

        final float hue;
        if (station.bikesCount() > 2 && station.vacantSlotsCount() > 2) {
            hue = BitmapDescriptorFactory.HUE_GREEN;
        } else if (station.bikesCount() > 0 && station.vacantSlotsCount() > 0) {
            hue = BitmapDescriptorFactory.HUE_YELLOW;
        } else {
            hue = BitmapDescriptorFactory.HUE_RED;
        }

        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(hue));
        return markerOptions;
    }

    private void updateViewOnMap() {
        if (viewOnMapObservable.station != null) {
            viewOnMap(viewOnMapObservable.station);
            viewOnMapObservable.done();
        }
    }

    public void viewOnMap(final Station station) {
        final GoogleMap googleMap = googleMapFragment.getMap();

        if (googleMap == null) {
            return;
        }

        final int zoom = 14;
        final CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(station.getLocation(), zoom);
        googleMap.animateCamera(cameraUpdate);

        final Marker marker = stationMarkerBiMap.get(station);

        if (marker != null) {
            marker.showInfoWindow();
        }
    }

    @Override
    public void onStationListUpdated() {
        getActivity().runOnUiThread(updateStationListAction);
    }

    @Override
    public void onStop() {
        final Model model = configuration.getModel();
        model.getStationsInfo().unregisterOnStationListUpdatedListener(this);
        viewOnMapObservable.deleteObserver(this);
        super.onStop();
    }

    @Override
    public void update(Observable observable, Object data) {
        assert observable == viewOnMapObservable;
        updateViewOnMap();
    }

    public void onViewOnMap(final Station station) {
        viewOnMapObservable.viewOnMap(station);
        viewOnMapObservable.notifyObservers();
    }

    @Override
    public void onInfoWindowClick(final Marker marker) {
        final Station station = stationMarkerBiMap.inverse().get(marker);

        FragmentManager fragmentManager = getFragmentManager();
        Fragment mainFragment = fragmentManager.findFragmentById(R.id.fragment_main);
        boolean phoneLayout = mainFragment != null && mainFragment.isAdded();

        if (phoneLayout) {
            Intent intent = new Intent(getActivity(), StationsActivity.class);
            intent.putExtra(StationsActivity.STATION_EXTRA, true);
            intent.putExtra(StationsActivity.STATION_ID_EXTRA, station.getId());
            startActivity(intent);
        } else {
            StationFragment stationFragment = new StationFragment();
            stationFragment.setArguments(new Bundle());
            stationFragment.getArguments().putString(StationFragment.STATION_ID_ARGUMENT, station.getId());

            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.remove(this);
            transaction.add(R.id.fragment_content, stationFragment);
            transaction.addToBackStack(null);
            transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            transaction.commit();
        }
    }

    @Override
    public boolean onMarkerClick(final Marker marker) {
        return false;
    }

    @Override
    public View getInfoWindow(final Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(final Marker marker) {
        final Station station = stationMarkerBiMap.inverse().get(marker);

        final LayoutInflater layoutInflater = getLayoutInflater(null);
        final View view = layoutInflater.inflate(R.layout.info_window_station, null);

        final TextView nameTextView = (TextView) view.findViewById(R.id.name_text);
        final TextView bikesCountTextView = (TextView) view.findViewById(R.id.bikes_count_text);
        final TextView vacantSlotsCountTextView = (TextView) view.findViewById(R.id.vacant_slots_count_text);

        nameTextView.setText(station.getName());

        showCount(bikesCountTextView, station.bikesCount());
        showCount(vacantSlotsCountTextView, station.vacantSlotsCount());

        return view;
    }

    private void showCount(TextView textView, int count) {
        textView.setTextColor(watermarkColors.colorFor(Watermark.fromCount(count)));
        textView.setText(Integer.toString(count));
    }

    @Override
    public void onStationListUpdateFailure() {
        // TODO
    }

    @Override
    public void onStationListUpdateFinished() {
        // TODO
    }

    @Override
    public void onStationListUpdateStarted() {
        // TODO
    }

    private static class ViewOnMapObservable extends Observable {
        private Station station;

        private void viewOnMap(Station station) {
            if (!station.equals(this.station)) {
                setChanged();
            }
            this.station = station;
        }

        public void done() {
            this.station = null;
        }
    }
}