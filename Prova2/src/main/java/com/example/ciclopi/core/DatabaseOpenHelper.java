package com.example.ciclopi.core;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseOpenHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "ciclopi";
    public static final int DATABASE_VERSION = 1;

    public static final String TABLE_STATION = "station";

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_LATITUDE = "latitude";
    public static final String COLUMN_LONGITUDE = "longitude";
    public static final String COLUMN_SLOTS = "slots";
    public static final String COLUMN_FAVORITE = "favorite";

    public static final String TABLE_LAST_UPDATE = "last_update";

    public static final String COLUMN_TIME = "time";

    public DatabaseOpenHelper(final Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(final SQLiteDatabase db) {
        db.beginTransaction();
        try {
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_LAST_UPDATE);
            db.execSQL("CREATE TABLE " + TABLE_LAST_UPDATE + " (" +
                    COLUMN_TIME + " INTEGER" +
                    ")");

            final ContentValues values = new ContentValues();
            values.put(COLUMN_TIME, 0);
            db.insert(TABLE_LAST_UPDATE, null, values);

            db.execSQL("DROP TABLE IF EXISTS " + TABLE_STATION);
            db.execSQL("CREATE TABLE " + TABLE_STATION + " (" +
                    COLUMN_ID + " TEXT PRIMARY KEY, " +
                    COLUMN_NAME + " TEXT, " +
                    COLUMN_LATITUDE + " REAL, " +
                    COLUMN_LONGITUDE + " REAL, " +
                    COLUMN_SLOTS + " TEXT, " +
                    COLUMN_FAVORITE + " INTEGER " +
                    ")");
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    @Override
    public void onUpgrade(final SQLiteDatabase db, final int oldVersion, final int newVersion) {
        onCreate(db);
    }
}
