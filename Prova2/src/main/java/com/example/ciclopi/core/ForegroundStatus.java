package com.example.ciclopi.core;

public class ForegroundStatus {
    private volatile boolean foreground;

    public void setForeground(boolean foreground) {
        this.foreground = foreground;
    }

    public boolean isForeground() {
        return foreground;
    }
}
