package com.example.ciclopi.core;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;

import com.example.ciclopi.model.MyLocationInfo;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;

public class LocationUpdater implements GooglePlayServicesClient.ConnectionCallbacks, GooglePlayServicesClient.OnConnectionFailedListener {
    public static final long FOREGROUND_UPDATE_INTERVAL_MILLIS = 5 * 60 * 1000;
    public static final long BACKGROUND_UPDATE_INTERVAL_MILLIS = 5 * 60 * 1000;

    private ForegroundStatus foregroundStatus;
    private final MyLocationInfo myLocationInfo;
    private final LocationClient locationClient;

    private LocationListener backgroundListener = new MyLocationListener();
    private LocationListener foregroundListener = new MyLocationListener();

    public LocationUpdater(Context context, ForegroundStatus foregroundStatus, MyLocationInfo myLocationInfo) {
        this.myLocationInfo = myLocationInfo;
        this.foregroundStatus = foregroundStatus;

        locationClient = new LocationClient(context, this, this);
        locationClient.connect();
    }

    private void startBackground() {
        final LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_NO_POWER);
        locationRequest.setInterval(BACKGROUND_UPDATE_INTERVAL_MILLIS);

        locationClient.requestLocationUpdates(locationRequest, backgroundListener);
    }

    private void stopBackground() {
        locationClient.removeLocationUpdates(backgroundListener);
    }

    public void onForeground() {
        if (!locationClient.isConnected()) {
            // will be done onConnect
            return;
        }

        startForeground();
    }

    private void startForeground() {
        final LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(FOREGROUND_UPDATE_INTERVAL_MILLIS);

        locationClient.requestLocationUpdates(locationRequest, foregroundListener);
    }

    public void onBackground() {
        if (!locationClient.isConnected()) {
            return;
        }

        locationClient.removeLocationUpdates(foregroundListener);
    }

    @Override
    public void onConnected(Bundle bundle) {
        startBackground();
        if (foregroundStatus.isForeground()) {
            startForeground();
        }
    }

    @Override
    public void onDisconnected() {
        myLocationInfo.setLocationUnknown();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        myLocationInfo.setLocationUnknown();
    }

    private class MyLocationListener implements LocationListener {

        @Override
        public void onLocationChanged(final Location location) {
            myLocationInfo.setLocation(location);
        }
    }
}
