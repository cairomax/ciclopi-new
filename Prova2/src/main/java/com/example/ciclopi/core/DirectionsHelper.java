package com.example.ciclopi.core;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;

import com.example.ciclopi.R;
import com.example.ciclopi.model.Station;
import com.google.android.gms.maps.model.LatLng;

public class DirectionsHelper {
    public static final int CHOICE_DIRECTIONS = 0;
    public static final int CHOICE_NAVIGATE = 1;

    private final Context context;

    public DirectionsHelper(Context context) {
        this.context = context;
    }

    public void showDirections(final Station station) {
        final LatLng location = station.getLocation();
        final Intent intent = new Intent(Intent.ACTION_VIEW, makeShowDirectionsUri(location));
        intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
        context.startActivity(intent);
    }

    private Uri makeShowDirectionsUri(final LatLng location) {
        return Uri.parse("http://maps.google.com/maps?daddr=" + location.latitude + "," + location.longitude + "&dirflg=w");
    }

    public void startNavigation(final Station station) {
        final LatLng location = station.getLocation();
        context.startActivity(new Intent(Intent.ACTION_VIEW, makeGoogleNavigationUri(location)));
    }

    private Uri makeGoogleNavigationUri(final LatLng location) {
        return Uri.parse("google.navigation:ll=" + location.latitude + "," + location.longitude + "&mode=w");
    }

    public void showDirectionsDialog(final Station station) {
        new AlertDialog.Builder(context).setItems(R.array.station_info_window_menu_items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialog, final int which) {
                switch (which) {
                    case CHOICE_DIRECTIONS:
                        showDirections(station);
                        break;
                    case CHOICE_NAVIGATE:
                        startNavigation(station);
                        break;
                    default:
                        throw new AssertionError();
                }
            }
        }).show();
    }
}
