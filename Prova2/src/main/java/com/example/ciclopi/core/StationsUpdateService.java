package com.example.ciclopi.core;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.IBinder;

import com.example.ciclopi.model.Model;
import com.example.ciclopi.model.Station;
import com.example.ciclopi.model.StationsInfo;
import com.example.ciclopi.preferences.PreferenceKeys;
import com.google.common.collect.ImmutableSet;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class StationsUpdateService extends Service implements SharedPreferences.OnSharedPreferenceChangeListener {
    public static final String FOREGROUND_EXTRA = "FOREGROUND";
    public static final String REFRESH_EXTRA = "REFRESH";
    public static final String MANUAL_REFRESH_EXTRA = "MANUAL_REFRESH";
    public static final String BACKGROUND_REFRESH_EXTRA = "BACKGROUND_REFRESH";

    public static final ImmutableSet<String> WATCHED_PREFERENCES = ImmutableSet.of(PreferenceKeys.AUTO_REFRESH_PREFERENCE,
            PreferenceKeys.BACKGROUND_REFRESH_PREFERENCE,
            PreferenceKeys.REFRESH_INTERVAL_PREFERENCE,
            PreferenceKeys.BACKGROUND_REFRESH_INTERVAL_PREFERENCE);
    public static final int MILLIS_IN_SECOND = 1000;
    public static final String DEFAULT_REFRESH_INTERVAL = "900";

    private Configuration configuration;

    private PendingIntent automaticRefreshPendingIntent;
    private PendingIntent backgroundRefreshPendingIntent;

    private AlarmManager alarmManager;
    private ForegroundStatus foregroundStatus;

    @Override
    public void onCreate() {
        super.onCreate();

        configuration = Configuration.forContext(this);

        Intent automaticRefreshIntent = new Intent(this, StationsUpdateService.class);
        automaticRefreshIntent.putExtra(REFRESH_EXTRA, true);
        automaticRefreshPendingIntent = PendingIntent.getService(this, 0, automaticRefreshIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent backgroundRefreshIntent = new Intent(this, StationsUpdateService.class);
        backgroundRefreshIntent.putExtra(REFRESH_EXTRA, true);
        backgroundRefreshIntent.putExtra(BACKGROUND_REFRESH_EXTRA, true);
        backgroundRefreshPendingIntent = PendingIntent.getService(this, 0, backgroundRefreshIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        foregroundStatus = configuration.getForegroundStatus();

        alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);

        if (!configuration.getModel().getStationsInfo().getStationsIfLoaded().isPresent()) {
            readStationsFromDb();
            scheduleRefresh(false);
        }

        configuration.getPreferences().registerOnSharedPreferenceChangeListener(this);
        updateAutomaticRefreshAlarm();
    }

    private void scheduleRefresh(boolean manual) {
        new StationsUpdateTask(this, manual) {
            @Override
            protected void onPostExecute(final Void aVoid) {
                writeStationsToDb();
            }
        }.execute();
    }

    private void writeStationsToDb() {
        final SQLiteDatabase db = new DatabaseOpenHelper(this).getWritableDatabase();

        if (db == null) {
            return;
        }

        try {
            writeStationsToDb(db);
        } finally {
            db.close();
        }
    }

    private void writeStationsToDb(SQLiteDatabase db) {
        final Model model = configuration.getModel();
        final StationsInfo stationsInfo = model.getStationsInfo();

        db.beginTransaction();
        try {
            db.delete(DatabaseOpenHelper.TABLE_STATION, null, null);

            final Collection<Station> stations = stationsInfo.getStationsIfLoaded().or(Collections.EMPTY_MAP).values();
            for (Station station : stations) {
                final ContentValues values = station.asContentValues();
                db.insert(DatabaseOpenHelper.TABLE_STATION, null, values);
            }

            final ContentValues lastUpdateValues = new ContentValues();
            lastUpdateValues.put(DatabaseOpenHelper.COLUMN_TIME, stationsInfo.getUpdatedTimeIfLoaded().or(Long.MAX_VALUE));
            db.update(DatabaseOpenHelper.TABLE_LAST_UPDATE, lastUpdateValues, null, null);

            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    private void readStationsFromDb() {
        final SQLiteDatabase db = new DatabaseOpenHelper(this).getReadableDatabase();

        if (db == null) {
            return;
        }

        try {
            readStationsFromDb(db);
        } finally {
            db.close();
        }
    }

    private void readStationsFromDb(SQLiteDatabase db) {
        final Model model = configuration.getModel();
        final StationsInfo stationsInfo = model.getStationsInfo();

        final Cursor cursor = db.query(DatabaseOpenHelper.TABLE_STATION, null, null, null, null, null, null);

        final ArrayList<Station> stations = new ArrayList<Station>();
        while (cursor.moveToNext()) {
            stations.add(Station.fromCursor(cursor));
        }

        final Cursor lastUpdateCursor = db.query(DatabaseOpenHelper.TABLE_LAST_UPDATE, null, null, null, null, null, null);
        lastUpdateCursor.moveToNext();
        final long updatedTime = lastUpdateCursor.getLong(lastUpdateCursor.getColumnIndex(DatabaseOpenHelper.COLUMN_TIME));

        stationsInfo.update(stations, updatedTime);
    }

    @Override
    public int onStartCommand(final Intent intent, final int flags, final int startId) {
        processRefreshIntent(intent);
        processForegroundIntent(intent);
        maybeStopForegroundRefreshes();

        return START_STICKY;
    }

    private void processRefreshIntent(Intent intent) {
        if (intent == null || !intent.getBooleanExtra(REFRESH_EXTRA, false)) {
            return;
        }

        boolean isManual = intent.getBooleanExtra(MANUAL_REFRESH_EXTRA, false);
        boolean isBackgroundRefresh = intent.getBooleanExtra(BACKGROUND_REFRESH_EXTRA, false);

        if (isManual || isBackgroundRefresh) {
            scheduleRefresh(isManual);
        }
    }

    private void processForegroundIntent(Intent intent) {
        if (intent == null || !intent.getBooleanExtra(FOREGROUND_EXTRA, false)) {
            return;
        }

        updateForegroundRefresh();
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        configuration.getPreferences().unregisterOnSharedPreferenceChangeListener(this);

        super.onDestroy();
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (!WATCHED_PREFERENCES.contains(key)) {
            // not interesting
            return;
        }

        updateAutomaticRefreshAlarm();
    }

    private void updateAutomaticRefreshAlarm() {
        updateBackgroundRefresh();
        updateForegroundRefresh();
    }

    private void updateForegroundRefresh() {
        SharedPreferences preferences = configuration.getPreferences();
        boolean automaticRefresh = preferences.getBoolean(PreferenceKeys.AUTO_REFRESH_PREFERENCE, false);
        int refreshIntervalSeconds = Integer.valueOf(preferences.getString(PreferenceKeys.REFRESH_INTERVAL_PREFERENCE, DEFAULT_REFRESH_INTERVAL));

        alarmManager.cancel(automaticRefreshPendingIntent);
        if (automaticRefresh && foregroundStatus.isForeground()) {
            alarmManager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME, 0, refreshIntervalSeconds * MILLIS_IN_SECOND, automaticRefreshPendingIntent);
        }
    }

    private void maybeStopForegroundRefreshes() {
        SharedPreferences preferences = configuration.getPreferences();
        boolean automaticRefresh = preferences.getBoolean(PreferenceKeys.AUTO_REFRESH_PREFERENCE, false);
        if (!automaticRefresh || !foregroundStatus.isForeground()) {
            alarmManager.cancel(automaticRefreshPendingIntent);
        }
    }

    private void updateBackgroundRefresh() {
        final SharedPreferences preferences = configuration.getPreferences();
        boolean backgroundRefresh = preferences.getBoolean(PreferenceKeys.BACKGROUND_REFRESH_PREFERENCE, false);
        int backgroundRefreshIntervalSeconds = Integer.valueOf(preferences.getString(PreferenceKeys.BACKGROUND_REFRESH_INTERVAL_PREFERENCE, "3600"));

        alarmManager.cancel(backgroundRefreshPendingIntent);

        if (backgroundRefresh) {
            alarmManager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME, 0, backgroundRefreshIntervalSeconds * MILLIS_IN_SECOND, backgroundRefreshPendingIntent);
        }
    }
}
