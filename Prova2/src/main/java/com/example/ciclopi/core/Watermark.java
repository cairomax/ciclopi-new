package com.example.ciclopi.core;

import com.google.common.base.Preconditions;

public enum Watermark {
    GOOD,
    LOW,
    CRITICAL;

    public static Watermark fromCount(int count) {
        Preconditions.checkArgument(0 <= count);

        if(count == 0) {
            return CRITICAL;
        }

        if(count <= 3) {
            return LOW;
        }

        return GOOD;
    }

    public static Watermark fromWalkingDistanceMeters(double meters) {
        if(meters < 500) {
            return GOOD;
        }

        if(meters < 1000) {
            return LOW;
        }

        return CRITICAL;
    }
}
