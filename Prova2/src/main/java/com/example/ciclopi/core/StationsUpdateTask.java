package com.example.ciclopi.core;

import android.content.Context;
import android.os.AsyncTask;

import com.example.ciclopi.model.StationsInfo;
import com.google.common.base.Charsets;
import com.google.common.io.CharStreams;
import com.example.ciclopi.model.Model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;

public class StationsUpdateTask extends AsyncTask<Void, Void, Void> {
    private static final URL ENDPOINT;
    private static final URL MANUAL_ENDPOINT;

    private final boolean manual;
    private final Context context;
    private final Configuration configuration;

    static {
        try {
            ENDPOINT = new URL("http://ciclopi.herokuapp.com/stations");
            MANUAL_ENDPOINT = new URL("http://ciclopi.herokuapp.com/stations?refresh=true");
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    public StationsUpdateTask(final Context context, final boolean manual) {
        this.context = context;
        configuration = Configuration.forContext(context);
        this.manual = manual;
    }

    @Override
    protected Void doInBackground(final Void... params) {
        updateIfNeeded();
        return null;
    }

    private void updateIfNeeded() {
        final Model model = configuration.getModel();
        final StationsInfo stationsInfo = model.getStationsInfo();

        boolean shouldUpdate = stationsInfo.takeUpdate();

        if(!shouldUpdate) {
            return;
        }

        try {
            update();
        } catch (IOException e) {
            e.printStackTrace();
            stationsInfo.onUpdateFailure();
        } catch (JSONException e) {
            e.printStackTrace();
            stationsInfo.onUpdateFailure();
        } finally {
            stationsInfo.onUpdateCompleted();
        }
    }

    private void update() throws IOException, JSONException {
        final Model model = configuration.getModel();
        final StationsInfo stationsInfo = model.getStationsInfo();

        URL endpoint = manual ? MANUAL_ENDPOINT : ENDPOINT;

        final String stationsInfoString;
        final HttpURLConnection connection = (HttpURLConnection) endpoint.openConnection();
        try {
            final InputStream stream = connection.getInputStream();
            final InputStreamReader reader = new InputStreamReader(stream, Charsets.UTF_8);
            stationsInfoString = CharStreams.toString(reader);
        } finally {
            connection.disconnect();
        }
        final JSONObject stationsInfoJsonObject = new JSONObject(stationsInfoString);

        if(connection.getDate() != 0) {
            long serverTimeOffset = connection.getDate() - new Date().getTime();
            model.getServerClock().updateServerTimeOffset(serverTimeOffset);
        }
        stationsInfo.updateFromJson(stationsInfoJsonObject);
    }
}
