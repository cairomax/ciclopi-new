package com.example.ciclopi.core;

import android.content.Context;
import android.content.res.Resources;
import android.text.format.DateUtils;

import com.example.ciclopi.R;

import java.text.DateFormat;
import java.util.Date;

public class LastUpdateFormatter {
    private final Context context;

    public LastUpdateFormatter(Context context) {
        this.context = context;
    }

    public CharSequence format(long updateTime, long now, boolean accurate) {
        Resources resources = context.getResources();

        long lag = now - updateTime;
        int secondsAgo = (int) lag / 1000;

        if (secondsAgo < 10) {
            return resources.getString(R.string.last_update_now);
        }

        if(accurate) {
            if (secondsAgo < 60) {
                int friendlySecondsAgo = (secondsAgo / 10) * 10;
                String format = resources.getQuantityString(R.plurals.last_update_seconds_ago, friendlySecondsAgo);
                return String.format(format, friendlySecondsAgo);
            }

            if (secondsAgo < 60 * 60) {
                int minutesAgo = secondsAgo / 60;
                String format = resources.getQuantityString(R.plurals.last_update_minutes_ago, minutesAgo);
                return String.format(format, minutesAgo);
            }
        }

        if(DateUtils.isToday(updateTime)) {
            String updateTimeString = DateFormat.getTimeInstance(DateFormat.SHORT).format(new Date(updateTime));
            return String.format(resources.getString(R.string.last_update_at_time_format), updateTimeString);
        } else {
            String updateDateTimeString = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.MEDIUM).format(new Date(updateTime));
            return String.format(resources.getString(R.string.last_update_on_date_format), updateDateTimeString);
        }
    }
}
