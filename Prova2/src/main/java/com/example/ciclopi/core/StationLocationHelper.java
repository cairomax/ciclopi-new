package com.example.ciclopi.core;

import android.location.Location;
import android.view.View;
import android.widget.TextView;

import com.example.ciclopi.model.MyLocationInfo;
import com.example.ciclopi.model.Station;
import com.example.ciclopi.station.StationCompassView;

public class StationLocationHelper {
    public void showDistance(TextView view, MyLocationInfo myLocationInfo, Station station) {
        final Location myLocation = myLocationInfo.getMyLocationIfAvailable().orNull();
        if (myLocation != null) {
            double distance = myLocation.distanceTo(getLocationOf(station));
            if (distance >= 1000f) {
                view.setText(String.format("%1.1f km", distance / 1000f));
            } else {
                view.setText(String.format("%2.0f0 m", distance / 10f));
            }
            view.setVisibility(View.VISIBLE);
        } else {
            view.setVisibility(View.INVISIBLE);
        }
    }

    public void showBearing(StationCompassView view, MyLocationInfo myLocationInfo, Station station) {
        final Location myLocation = myLocationInfo.getMyLocationIfAvailable().orNull();
        final double myOrientation = myLocationInfo.getOrientation();

        if (myLocation != null) {
            final double bearing = computeBearing(myLocation, myOrientation, getLocationOf(station));
            view.setBearing(bearing);
        } else {
            view.setBearingUnknown();
        }
    }

    private double computeBearing(Location myLocation, double myOrientation, Location stationLocation) {
        double expectedBearing = myLocation.bearingTo(stationLocation);
        double actualBearing = myLocation.hasBearing() ? myLocation.getBearing() : myOrientation;
        return expectedBearing - actualBearing;
    }

    public Location getLocationOf(Station station) {
        Location stationLocation = new Location("");
        stationLocation.setLatitude(station.getLocation().latitude);
        stationLocation.setLongitude(station.getLocation().longitude);
        return stationLocation;
    }
}
