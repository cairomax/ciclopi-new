package com.example.ciclopi.core;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;

import com.example.ciclopi.R;
import com.example.ciclopi.model.SlotState;
import com.example.ciclopi.model.Station;

import java.util.List;

public class SlotsTableHelper {
    private final Context context;
    private final int slotsTableColumns;

    public SlotsTableHelper(Context context) {
        this.context = context;

        slotsTableColumns = context.getResources().getInteger(R.integer.slots_table_columns);
    }

    public void fillSlotsTable(Station station, TableLayout tableLayout, LayoutInflater layoutInflater) {
        final List<SlotState> slotStates = station.getSlotStates();
        final int totalSlots = slotStates.size();

        int rows = (totalSlots + slotsTableColumns - 1) / slotsTableColumns;

        tableLayout.removeAllViews();

        for (int row = 0; row < rows; row++) {
            final TableRow tableRow = new TableRow(context);
            final int firstSlotInRow = row * slotsTableColumns;

            for (int column = 0; column < slotsTableColumns; column++) {
                final int slotIndex = firstSlotInRow + column;
                if (slotIndex >= slotStates.size()) {
                    break;
                }

                final SlotState slotState = slotStates.get(slotIndex);
                final int imageResourceId;
                switch (slotState) {
                    case BIKE:
                        imageResourceId = R.drawable.ic_slot_bike;
                        break;
                    case VACANT:
                        imageResourceId = R.drawable.ic_slot_vacant;
                        break;
                    default:
                        imageResourceId = R.drawable.ic_slot_unknown;
                        break;
                }
                final ImageView slotView = (ImageView) layoutInflater.inflate(R.layout.view_slot, null);
                slotView.setImageResource(imageResourceId);
                tableRow.addView(slotView);
            }
            TableLayout.LayoutParams layoutParams = new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParams.weight = 1;
            tableRow.setLayoutParams(layoutParams);
            tableLayout.addView(tableRow);
        }

        tableLayout.setStretchAllColumns(true);
        tableLayout.setShrinkAllColumns(true);
        tableLayout.setWeightSum(rows);
    }
}
