package com.example.ciclopi.core;

import android.content.Context;
import android.content.res.Resources;

import com.example.ciclopi.R;

public class WatermarkColors {
    private final int goodColor;
    private final int lowColor;
    private final int criticalColor;

    public WatermarkColors(Context context) {
        Resources resources = context.getResources();

        goodColor = resources.getColor(R.color.watermark_good);
        lowColor = resources.getColor(R.color.watermark_low);
        criticalColor = resources.getColor(R.color.watermark_critical);
    }

    public int colorFor(Watermark watermark) {
        switch (watermark) {
            case GOOD:
                return goodColor;
            case LOW:
                return lowColor;
            case CRITICAL:
                return criticalColor;
            default:
                throw new AssertionError();
        }
    }
}
