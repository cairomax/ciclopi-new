package com.example.ciclopi.core;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;

import com.example.ciclopi.model.Model;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class Configuration {
    private static final LoadingCache<Context, Configuration> CONFIGURATION_CACHE = CacheBuilder.newBuilder().weakKeys().build(new CacheLoader<Context, Configuration>() {
        @Override
        public Configuration load(final Context context) {
            return new Configuration(context);
        }
    });

    private static final LoadingCache<Context, ApplicationConfiguration> APPLICATION_CONFIGURATION_CACHE = CacheBuilder.newBuilder().weakKeys().build(new CacheLoader<Context, ApplicationConfiguration>() {
        @Override
        public ApplicationConfiguration load(final Context context) {
            return new ApplicationConfiguration(context);
        }
    });
    public static final int FOREGROUND_CHECK_LEAP_TIME_MILLIS = 10000;
    private Runnable foregroundCheckAction = new Runnable() {
        @Override
        public void run() {
            if (applicationConfiguration.foregroundActivities.get() == 0) {
                boolean wasForeground = applicationConfiguration.foreground.getAndSet(false);
                if (wasForeground) {
                    onBackground();
                }
            } else {
                boolean wasForeground = applicationConfiguration.foreground.getAndSet(true);
                if (!wasForeground) {
                    onForeground();
                }
            }
        }
    };

    public static Configuration forContext(Context context) {
        return CONFIGURATION_CACHE.getUnchecked(context);
    }

    private static final StationLocationHelper STATION_LOCATION_HELPER = new StationLocationHelper();

    private final ApplicationConfiguration applicationConfiguration;

    private final Context context;

    private final SharedPreferences sharedPreferences;
    private final SlotsTableHelper slotsTableHelper;
    private final WatermarkColors watermarkColors;
    private final DirectionsHelper directionsHelper;
    private final LastUpdateFormatter lastUpdateFormatter;

    private Configuration(Context context) {
        this.context = context;

        applicationConfiguration = APPLICATION_CONFIGURATION_CACHE.getUnchecked(context.getApplicationContext());
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        slotsTableHelper = new SlotsTableHelper(context);
        watermarkColors = new WatermarkColors(context);
        directionsHelper = new DirectionsHelper(context);
        lastUpdateFormatter = new LastUpdateFormatter(context);
    }

    public Model getModel() {
        return applicationConfiguration.model;
    }

    public SharedPreferences getPreferences() {
        return sharedPreferences;
    }

    public void onStart(Activity activity) {
        int previousForegroundActivities = applicationConfiguration.foregroundActivities.getAndIncrement();
        if (previousForegroundActivities == 0) {
            scheduleForegroundCheck();
        }
    }

    public void onStop(Activity activity) {
        int currentForegroundActivities = applicationConfiguration.foregroundActivities.decrementAndGet();
        if (currentForegroundActivities == 0) {
            scheduleForegroundCheck();
        }
    }

    private void scheduleForegroundCheck() {
        applicationConfiguration.handler.postDelayed(foregroundCheckAction, FOREGROUND_CHECK_LEAP_TIME_MILLIS);
    }

    private void onForeground() {
        getForegroundStatus().setForeground(true);
        applicationConfiguration.locationUpdater.onForeground();
        startForegroundRefreshes();
    }

    private void startForegroundRefreshes() {
        Intent intent = new Intent(context, StationsUpdateService.class);
        intent.putExtra(StationsUpdateService.FOREGROUND_EXTRA, true);
        context.startService(intent);
    }

    private void onBackground() {
        getForegroundStatus().setForeground(false);
        applicationConfiguration.locationUpdater.onBackground();
    }

    public ForegroundStatus getForegroundStatus() {
        return applicationConfiguration.foregroundStatus;
    }

    public StationLocationHelper getStationLocationHelper() {
        return STATION_LOCATION_HELPER;
    }

    public SlotsTableHelper getSlotsTableHelper() {
        return slotsTableHelper;
    }

    public DirectionsHelper getDirectionsHelper() {
        return directionsHelper;
    }

    public LastUpdateFormatter getLastUpdateFormatter() {
        return lastUpdateFormatter;
    }

    public WatermarkColors getWatermarkColors() {
        return watermarkColors;
    }

    static class ApplicationConfiguration {
        private final AtomicInteger foregroundActivities = new AtomicInteger();
        private final Model model;
        private final LocationUpdater locationUpdater;
        private final ForegroundStatus foregroundStatus;
        private final Handler handler;
        private final AtomicBoolean foreground = new AtomicBoolean();

        public ApplicationConfiguration(Context applicationContext) {
            this.model = new Model(applicationContext);
            this.foregroundStatus = new ForegroundStatus();
            this.locationUpdater = new LocationUpdater(applicationContext, foregroundStatus, model.getMyLocationInfo());
            handler = new Handler(Looper.getMainLooper());
        }
    }
}
