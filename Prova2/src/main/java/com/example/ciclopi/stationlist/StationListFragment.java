package com.example.ciclopi.stationlist;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.ciclopi.R;
import com.example.ciclopi.StationsActivity;
import com.example.ciclopi.core.Configuration;
import com.example.ciclopi.core.StationLocationHelper;
import com.example.ciclopi.core.StationsUpdateService;
import com.example.ciclopi.core.Watermark;
import com.example.ciclopi.core.WatermarkColors;
import com.example.ciclopi.map.CiclopiMapFragment;
import com.example.ciclopi.model.Model;
import com.example.ciclopi.model.MyLocationInfo;
import com.example.ciclopi.model.ServerClock;
import com.example.ciclopi.model.Station;
import com.example.ciclopi.model.StationsInfo;
import com.example.ciclopi.station.StationFragment;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

public class StationListFragment extends Fragment implements ServerClock.OnTickListener, StationsInfo.OnStationListUpdateListener, MyLocationInfo.MyLocationUpdateListener, AdapterView.OnItemClickListener {
    private StationListViewHolder views;

    private Configuration configuration;
    private WatermarkColors watermarkColors;

    private ArrayAdapter<Station> listAdapter;

    private final Runnable updateAction = new Runnable() {
        @Override
        public void run() {
            updateAll();
        }
    };

    private StationsInfo stationsInfo;
    private Model model;
    private StationLocationHelper stationLocationHelper;

    @Override
    public void onAttach(final Activity activity) {
        super.onAttach(activity);

        configuration = Configuration.forContext(activity);
        watermarkColors = configuration.getWatermarkColors();
        stationLocationHelper = configuration.getStationLocationHelper();

        model = configuration.getModel();
        stationsInfo = model.getStationsInfo();
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_station_list, null);
    }

    @Override
    public void onActivityCreated(final Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        views = StationListViewHolder.forView(getView());

        setUpListView();
    }

    private void setUpListView() {
        listAdapter = new ArrayAdapter<Station>(getActivity(), 0) {
            @Override
            public View getView(final int position, View convertView, final ViewGroup parent) {
                final Station station = getItem(position);

                StationRowViewHolder rowViewHolder = StationRowViewHolder.cached(convertView);

                if (rowViewHolder == null) {
                    final View itemView = getActivity().getLayoutInflater().inflate(R.layout.row_station, null);
                    rowViewHolder = StationRowViewHolder.forView(itemView);
                }

                updateStationRow(station, rowViewHolder);

                return rowViewHolder.rootView;
            }

            @Override
            public boolean hasStableIds() {
                return true;
            }
        };
        listAdapter.setNotifyOnChange(false);

        views.listView.setAdapter(listAdapter);
        views.listView.setOnItemClickListener(this);
    }

    private void updateAll() {
        if (!stationsInfo.getStationsIfLoaded().isPresent()) {
            showBillboard();
        } else {
            showList();
            updateStationList();
        }
    }

    private void showBillboard() {
        views.listView.setVisibility(View.GONE);

        boolean updating = stationsInfo.isUpdating();

        views.billboardProgressBar.setVisibility(updating ? View.VISIBLE : View.INVISIBLE);
        views.billboardTextView.setVisibility(updating ? View.INVISIBLE : View.VISIBLE);
    }

    private void showList() {
        views.billboardProgressBar.setVisibility(View.GONE);
        views.billboardTextView.setVisibility(View.GONE);

        views.listView.setVisibility(View.VISIBLE);
    }

    private void updateStationList() {
        listAdapter.clear();

        if (!stationsInfo.getStationsIfLoaded().isPresent()) {
            return;
        }

        Collection<Station> stations = stationsInfo.getStationsIfLoaded().get().values();
        ArrayList<Station> sortedStations = sortStations(stations);

        for (Station station : sortedStations) {
            listAdapter.add(station);
        }

        listAdapter.notifyDataSetChanged();
    }

    private ArrayList<Station> sortStations(Collection<Station> stations) {
        ArrayList<Station> sortedStations = new ArrayList<Station>(stations);
        final MyLocationInfo myLocationInfo = model.getMyLocationInfo();
        if (myLocationInfo.getMyLocationIfAvailable().isPresent()) {
            final Location myLocation = myLocationInfo.getMyLocationIfAvailable().get();

            Collections.sort(sortedStations, new Comparator<Station>() {
                @Override
                public int compare(Station station1, Station station2) {
                    double distance1 = myLocation.distanceTo(stationLocationHelper.getLocationOf(station1));
                    double distance2 = myLocation.distanceTo(stationLocationHelper.getLocationOf(station2));
                    return Double.compare(distance1, distance2);
                }
            });
        }
        return sortedStations;
    }

    private void updateStationRow(final Station station, final StationRowViewHolder rowViewHolder) {
        setupRowCallbacks(station, rowViewHolder);
        rowViewHolder.station = station;
        updateRowName(station, rowViewHolder);
        updateRowCounters(station, rowViewHolder);
        updateButtonsVisibility(rowViewHolder);
        updateDirections(rowViewHolder, station);
    }

    private void updateDirections(StationRowViewHolder rowViewHolder, Station station) {
        if (!isContentFragmentVisible()) {
            rowViewHolder.stationCompass.setVisibility(View.VISIBLE);
            rowViewHolder.distanceText.setVisibility(View.VISIBLE);

            MyLocationInfo myLocationInfo = model.getMyLocationInfo();

            stationLocationHelper.showDistance(rowViewHolder.distanceText, myLocationInfo, station);
            stationLocationHelper.showBearing(rowViewHolder.stationCompass, myLocationInfo, station);
        } else {
            rowViewHolder.stationCompass.setVisibility(View.GONE);
            rowViewHolder.distanceText.setVisibility(View.GONE);
        }
    }

    private void updateButtonsVisibility(StationRowViewHolder rowViewHolder) {
        rowViewHolder.viewOnMapButton.setVisibility(!isContentFragmentVisible() ? View.VISIBLE : View.GONE);
        // rowViewHolder.logoView.setVisibility(!isMapFragmentVisible() ? View.VISIBLE : View.GONE);
    }

    private void setupRowCallbacks(final Station station, final StationRowViewHolder rowViewHolder) {
        rowViewHolder.viewOnMapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                onViewOnMapClicked(station);
            }
        });
    }

    private void onViewOnMapClicked(final Station station) {
        if (isMapFragmentVisible()) {
            viewOnMap(station);
        } else {
            openMap(station);
        }
    }

    private void openMap(Station station) {
        Intent intent = new Intent(getActivity(), StationsActivity.class);
        intent.putExtra(StationsActivity.MAP_EXTRA, true);
        intent.putExtra(StationsActivity.STATION_ID_EXTRA, station.getId());
        startActivity(intent);
    }

    private boolean isMapFragmentVisible() {
        final Fragment mapFragment = getMapFragment();
        return mapFragment != null && mapFragment.isVisible();
    }

    private CiclopiMapFragment getMapFragment() {
        return (CiclopiMapFragment) getFragmentManager().findFragmentByTag(StationsActivity.FRAGMENT_MAP_TAG);
    }

    private boolean isStationFragmentVisible() {
        final StationFragment fragment = getStationFragment();
        return fragment != null && fragment.isVisible();
    }

    private boolean isContentFragmentVisible() {
        return isMapFragmentVisible() || isStationFragmentVisible();
    }

    private StationFragment getStationFragment() {
        return (StationFragment) getFragmentManager().findFragmentByTag(StationsActivity.FRAGMENT_STATION_TAG);
    }

    private void updateRowName(final Station station, final StationRowViewHolder rowViewHolder) {
        rowViewHolder.nameTextView.setText(station.getName());
    }

    private void updateRowCounters(final Station station, final StationRowViewHolder rowViewHolder) {
        int bikes = station.bikesCount();
        int vacantSlots = station.vacantSlotsCount();

        showCount(rowViewHolder.bikesCountTextView, bikes);
        showCount(rowViewHolder.vacantSlotsCountTextView, vacantSlots);
    }

    private void showCount(TextView textView, int count) {
        textView.setText(Integer.toString(count));
        textView.setTextColor(watermarkColors.colorFor(Watermark.fromCount(count)));
    }

    @Override
    public void onStart() {
        super.onStart();

        getActivity().startService(new Intent(getActivity(), StationsUpdateService.class));

        stationsInfo.registerOnStationListUpdatedListener(this);
        model.getMyLocationInfo().registerMyLocationUpdateListener(this);
        model.getServerClock().addOnTickListener(this);

        updateAll();
    }

    private void invalidate() {
        getActivity().runOnUiThread(updateAction);
    }

    @Override
    public void onStationListUpdated() {
        invalidate();
    }

    @Override
    public void onMyLocationUpdated() {
        invalidate();
    }

    @Override
    public void onServerClockTick() {
        invalidate();
    }

    @Override
    public void onStationListUpdateStarted() {
        invalidate();
    }

    @Override
    public void onStationListUpdateFinished() {
        invalidate();
    }

    @Override
    public void onStationListUpdateFailure() {
    }

    @Override
    public void onStop() {
        stationsInfo.unregisterOnStationListUpdatedListener(this);
        model.getMyLocationInfo().unregisterMyLocationUpdateListener(this);
        model.getServerClock().removeOnTickListener(this);
        super.onStop();
    }

    @Override
    public void onItemClick(final AdapterView<?> parent, final View view, final int position, final long id) {
        final Station station = (Station) parent.getAdapter().getItem(position);

        if (isMapFragmentVisible()) {
            viewOnMap(station);
        } else if (isStationFragmentVisible()) {
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            StationFragment fragment = new StationFragment();
            fragment.setArguments(new Bundle());
            fragment.getArguments().putString(StationFragment.STATION_ID_ARGUMENT, station.getId());
            transaction.replace(R.id.fragment_content, fragment, StationsActivity.FRAGMENT_STATION_TAG);
            transaction.addToBackStack(null);
            transaction.commit();
        } else {
            Intent intent = new Intent(getActivity(), StationsActivity.class);
            intent.putExtra(StationsActivity.STATION_EXTRA, true);
            intent.putExtra(StationsActivity.STATION_ID_EXTRA, station.getId());
            startActivity(intent);
        }
    }

    private void viewOnMap(Station station) {
        getMapFragment().onViewOnMap(station);
    }
}