package com.example.ciclopi.stationlist;

import android.view.View;
import android.widget.ImageButton;
import android.widget.TableLayout;
import android.widget.TextView;

import com.example.ciclopi.R;
import com.example.ciclopi.model.Station;
import com.example.ciclopi.station.StationCompassView;

public class StationRowViewHolder {
    public final View rootView;
    public final TextView nameTextView;
    public final TextView bikesCountTextView;
    public final TextView vacantSlotsCountTextView;
    public final ImageButton viewOnMapButton;
    public final StationCompassView stationCompass;
    public final TextView distanceText;
    public final TableLayout slotsTableLayout;
    public final View logoView;

    public Station station;

    private StationRowViewHolder(final View rootView) {
        this.rootView = rootView;
        nameTextView = (TextView) rootView.findViewById(R.id.name_text);
        bikesCountTextView = (TextView) rootView.findViewById(R.id.bikes_count_text);
        vacantSlotsCountTextView = (TextView) rootView.findViewById(R.id.vacant_slots_count_text);
        viewOnMapButton = (ImageButton) rootView.findViewById(R.id.view_on_map_button);
        stationCompass = (StationCompassView) rootView.findViewById(R.id.station_compass);
        distanceText = (TextView) rootView.findViewById(R.id.distance_text);
        slotsTableLayout = (TableLayout) rootView.findViewById(R.id.slots_table_layout);
        logoView = rootView.findViewById(R.id.logo_view);

        rootView.setTag(this);
    }

    public static StationRowViewHolder forView(final View rootView) {
        return new StationRowViewHolder(rootView);
    }

    public static StationRowViewHolder cached(final View convertView) {
        if (convertView == null) {
            return null;
        }

        final Object tag = convertView.getTag();

        if (tag instanceof StationRowViewHolder) {
            return (StationRowViewHolder) tag;
        } else {
            return null;
        }
    }
}
