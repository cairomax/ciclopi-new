package com.example.ciclopi.stationlist;

import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.ciclopi.R;

public class StationListViewHolder {
    public final ListView listView;
    public final ProgressBar billboardProgressBar;
    public final TextView billboardTextView;

    private StationListViewHolder(View rootView) {
        listView = (ListView) rootView.findViewById(R.id.list_view);
        billboardProgressBar = (ProgressBar) rootView.findViewById(R.id.billboard_progress_bar);
        billboardTextView = (TextView) rootView.findViewById(R.id.billboard_text_view);
    }

    public static StationListViewHolder forView(View rootView) {
        return new StationListViewHolder(rootView);
    }
}