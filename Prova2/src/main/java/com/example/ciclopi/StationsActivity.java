package com.example.ciclopi;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.ciclopi.core.Configuration;
import com.example.ciclopi.core.StationsUpdateService;
import com.example.ciclopi.map.CiclopiMapFragment;
import com.example.ciclopi.model.Station;
import com.example.ciclopi.model.StationsInfo;
import com.example.ciclopi.preferences.CiclopiPreferencesActivity;
import com.example.ciclopi.station.StationFragment;
import com.example.ciclopi.stationlist.StationListFragment;

import java.util.Collections;

public class StationsActivity extends ActionBarActivity implements StationsInfo.OnStationListUpdateListener {
    public static final String MAP_EXTRA = "map";
    public static final String STATION_EXTRA = "station";
    public static final String STATION_ID_EXTRA = "station_id";

    public static final String FRAGMENT_MAP_TAG = "map";
    public static final String FRAGMENT_STATION_TAG = "station";

    private Configuration configuration;

    private final Runnable updateProgressAction = new Runnable() {
        @Override
        public void run() {
            updateProgress();
        }
    };

    private StationsInfo stationsInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        configuration = Configuration.forContext(this);
        stationsInfo = configuration.getModel().getStationsInfo();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setContentView(R.layout.activity_stations);
    }

    @Override
    protected void onStart() {
        super.onStart();
        configuration.onStart(this);

        if (hasPhoneLayout()) {
            setupPhoneLayout();
        } else {
            setupTabletLayout();
        }

        getSupportFragmentManager().executePendingTransactions();

        stationsInfo.registerOnStationListUpdatedListener(this);
        updateProgress();
    }

    @Override
    protected void onStop() {
        stationsInfo.unregisterOnStationListUpdatedListener(this);
        configuration.onStop(this);

        super.onStop();
    }

    private boolean hasPhoneLayout() {
        View mainFragmentContainer = findViewById(R.id.fragment_main);

        return mainFragmentContainer != null;
    }

    private void setupTabletLayout() {
        boolean showStation = getIntent().getBooleanExtra(STATION_EXTRA, false);

        Station station = getStation();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();

        Fragment oldStationFragment = fragmentManager.findFragmentByTag(FRAGMENT_STATION_TAG);

        if (oldStationFragment != null) {
            transaction.remove(oldStationFragment);
        }

        if (showStation) {
            StationFragment stationFragment = createStationFragment(station);
            transaction.add(R.id.fragment_content, stationFragment, FRAGMENT_STATION_TAG);
        } else {
            CiclopiMapFragment mapFragment = new CiclopiMapFragment();
            transaction.add(R.id.fragment_content, mapFragment, FRAGMENT_MAP_TAG);

            if (station != null) {
                mapFragment.onViewOnMap(station);
            }
        }
        transaction.commit();
    }

    private void setupPhoneLayout() {
        boolean showMap = getIntent().getBooleanExtra(MAP_EXTRA, false);
        boolean showStation = getIntent().getBooleanExtra(STATION_EXTRA, false);

        final Station station = getStation();

        if (showMap) {
            setupMapPhoneLayout(station);
        } else if (showStation && station != null) {
            setupStationPhoneLayout(station);
        } else {
            setupStationListPhoneLayout();
        }
    }

    private Station getStation() {
        final String stationId = getIntent().getStringExtra(STATION_ID_EXTRA);

        final Station station;
        if (stationId != null) {
            station = configuration.getModel().getStationsInfo().getStationsIfLoaded().or(Collections.EMPTY_MAP).get(stationId);
        } else {
            station = null;
        }
        return station;
    }

    private void setupMapPhoneLayout(Station station) {
        CiclopiMapFragment mapFragment = new CiclopiMapFragment();
        replaceMainFragment(mapFragment, FRAGMENT_MAP_TAG);

        if (station != null) {
            mapFragment.onViewOnMap(station);
        }
    }

    private void setupStationPhoneLayout(Station station) {
        StationFragment stationFragment = createStationFragment(station);
        replaceMainFragment(stationFragment, FRAGMENT_STATION_TAG);
    }

    private StationFragment createStationFragment(Station station) {
        StationFragment stationFragment = new StationFragment();
        stationFragment.setArguments(new Bundle());
        stationFragment.getArguments().putString(StationFragment.STATION_ID_ARGUMENT, station.getId());
        return stationFragment;
    }

    private void setupStationListPhoneLayout() {
        StationListFragment stationListFragment = new StationListFragment();
        replaceMainFragment(stationListFragment, null);
    }

    private void replaceMainFragment(Fragment stationFragment, String tag) {
        FragmentManager fragmentManager = getSupportFragmentManager();

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        fragmentTransaction.replace(R.id.fragment_main, stationFragment, tag);
        fragmentTransaction.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        boolean updating = stationsInfo.isUpdating();
        getMenuInflater().inflate(R.menu.stations, menu);

        if (updating) {
            MenuItem refreshMenuItem = menu.findItem(R.id.action_refresh);
            MenuItemCompat.setActionView(refreshMenuItem, R.layout.refreshing_action_view);
            refreshMenuItem.setEnabled(false);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh: {
                Intent intent = new Intent(this, StationsUpdateService.class);
                intent.putExtra(StationsUpdateService.REFRESH_EXTRA, true);
                intent.putExtra(StationsUpdateService.MANUAL_REFRESH_EXTRA, true);
                startService(intent);
                return true;
            }
            case R.id.action_settings: {
                Intent intent = new Intent(this, CiclopiPreferencesActivity.class);
                startActivity(intent);
                return true;
            }
            default:
                return false;
        }
    }

    private void updateProgress() {
        supportInvalidateOptionsMenu();
    }

    @Override
    public void onStationListUpdateStarted() {
        runOnUiThread(updateProgressAction);
    }

    @Override
    public void onStationListUpdateFailure() {
    }

    @Override
    public void onStationListUpdateFinished() {
        runOnUiThread(updateProgressAction);
    }

    @Override
    public void onStationListUpdated() {
    }
}
