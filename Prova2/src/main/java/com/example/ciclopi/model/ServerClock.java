package com.example.ciclopi.model;

import java.util.ArrayList;
import java.util.List;

public class ServerClock {
    private static final long TICK_INTERVAL_MILLIS = 5000;

    private final List<OnTickListener> listeners = new ArrayList<OnTickListener>();

    private long serverTimeOffset = Long.MAX_VALUE;
    private long lastTickTime;

    public void updateServerTimeOffset(long serverTimeOffset) {
        this.serverTimeOffset = serverTimeOffset;
    }

    public void checkTicks() {
        long time = getCurrentTime();
        if (time - lastTickTime >= TICK_INTERVAL_MILLIS) {
            tick();
            lastTickTime = time;
        }
    }

    private void tick() {
        for(OnTickListener listener : listeners) {
            listener.onServerClockTick();
        }
    }

    private boolean hasAccurateServerTimeOffset() {
        return serverTimeOffset != Long.MAX_VALUE;
    }

    public boolean isAccurate() {
        return hasAccurateServerTimeOffset();
    }

    public long getCurrentTime() {
        if(hasAccurateServerTimeOffset()) {
            return System.currentTimeMillis() + serverTimeOffset;
        } else {
            return System.currentTimeMillis();
        }
    }

    public void addOnTickListener(OnTickListener listener) {
        listeners.add(listener);
    }

    public void removeOnTickListener(OnTickListener listener) {
        listeners.remove(listener);
    }

    public interface OnTickListener {
        void onServerClockTick();
    }
}
