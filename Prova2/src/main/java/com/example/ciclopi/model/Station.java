package com.example.ciclopi.model;

import android.content.ContentValues;
import android.database.Cursor;

import com.example.ciclopi.core.DatabaseOpenHelper;
import com.google.android.gms.maps.model.LatLng;
import com.google.common.base.Joiner;
import com.google.common.base.Objects;
import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Station {
    private final String id;
    private String name = "";
    private LatLng location;
    private ImmutableList<SlotState> slotStates = ImmutableList.of();

    public Station(final String id) {
        this.id = id;
    }

    public static Station fromCursor(final Cursor cursor) {
        final String id = cursor.getString(cursor.getColumnIndex(DatabaseOpenHelper.COLUMN_ID));

        final Station station = new Station(id);

        station.name = cursor.getString(cursor.getColumnIndex(DatabaseOpenHelper.COLUMN_NAME));

        final double latitude = cursor.getDouble(cursor.getColumnIndex(DatabaseOpenHelper.COLUMN_LATITUDE));
        final double longitude = cursor.getDouble(cursor.getColumnIndex(DatabaseOpenHelper.COLUMN_LONGITUDE));

        station.location = new LatLng(latitude, longitude);

        final ImmutableList.Builder<SlotState> newSlotStates = ImmutableList.builder();
        final Iterable<String> slotStateStrings = Splitter.on("|").split(slotStatesJoinedString(cursor));
        for (String slotStateString : slotStateStrings) {
            newSlotStates.add(parseSlotState(slotStateString));
        }
        station.slotStates = newSlotStates.build();

        return station;
    }

    private static SlotState parseSlotState(final String slotStateString) {
        SlotState slotState;
        try {
            slotState = SlotState.valueOf(slotStateString);
        } catch (IllegalArgumentException e) {
            slotState = SlotState.UNKNOWN;
        }
        return slotState;
    }

    private static String slotStatesJoinedString(final Cursor cursor) {
        return cursor.getString(cursor.getColumnIndex(DatabaseOpenHelper.COLUMN_SLOTS));
    }

    public void updateFromJson(final JSONObject stationJsonObject) throws JSONException {
        update(fromJson(stationJsonObject));
    }

    public static Station fromJson(final JSONObject stationJsonObject) throws JSONException {
        final String id = stationJsonObject.getString("id");

        final Station station = new Station(id);
        station.name = stationJsonObject.getString("name");

        final double latitude = stationJsonObject.getDouble("latitude");
        final double longitude = stationJsonObject.getDouble("longitude");

        station.location = new LatLng(latitude, longitude);

        final ArrayList<SlotState> slotStates = new ArrayList<SlotState>();

        final JSONArray slotStatesJsonArray = stationJsonObject.getJSONArray("slotStates");
        for (int i = 0; i < slotStatesJsonArray.length(); i++) {
            final String slotStateString = slotStatesJsonArray.getString(i);
            SlotState slotState = parseSlotState(slotStateString);
            slotStates.add(slotState);
        }

        station.slotStates = ImmutableList.copyOf(slotStates);

        return station;
    }

    public void update(final Station newStation) {
        assert id.equals(newStation.id);

        if (!name.equals(newStation.name)) {
            name = newStation.name;
        }

        if (!Objects.equal(location, newStation.location)) {
            location = newStation.location;
        }

        if (!slotStates.equals(newStation.slotStates)) {
            slotStates = ImmutableList.copyOf(newStation.slotStates);
        }
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public LatLng getLocation() {
        return location;
    }

    public int slotsCount() {
        return getSlotStates().size();
    }

    public int bikesCount() {
        return countSlots(SlotState.BIKE);
    }

    public int vacantSlotsCount() {
        return countSlots(SlotState.VACANT);
    }

    public int unknownSlotsCount() {
        return slotsCount() - bikesCount() - vacantSlotsCount();
    }

    private int countSlots(SlotState searchedState) {
        int count = 0;
        for (SlotState slotState : getSlotStates()) {
            if (slotState == searchedState) {
                count++;
            }
        }
        return count;
    }

    public List<SlotState> getSlotStates() {
        return slotStates;
    }

    public ContentValues asContentValues() {
        final ContentValues values = new ContentValues();

        values.put(DatabaseOpenHelper.COLUMN_ID, id);
        values.put(DatabaseOpenHelper.COLUMN_NAME, name);
        values.put(DatabaseOpenHelper.COLUMN_LATITUDE, location.latitude);
        values.put(DatabaseOpenHelper.COLUMN_LONGITUDE, location.longitude);
        values.put(DatabaseOpenHelper.COLUMN_FAVORITE, false);
        values.put(DatabaseOpenHelper.COLUMN_SLOTS, formatSlotStates());

        return values;
    }

    private String formatSlotStates() {
        return Joiner.on("|").join(slotStates);
    }
}
