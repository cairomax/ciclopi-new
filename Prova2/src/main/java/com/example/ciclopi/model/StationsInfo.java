package com.example.ciclopi.model;

import com.google.common.base.Optional;
import com.google.common.base.Throwables;
import com.google.common.collect.Maps;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

public class StationsInfo {
    private final Map<String, Station> stations = Maps.newConcurrentMap();
    private final Optional<Map<String, Station>> optionalStationsPresent = Optional.of(Collections.unmodifiableMap(stations));

    private final List<OnStationListUpdateListener> listeners = new ArrayList<OnStationListUpdateListener>();

    private long updatedServerTime;
    private AtomicBoolean updating = new AtomicBoolean();

    public Optional<Map<String, Station>> getStationsIfLoaded() {
        if (stations.isEmpty()) {
            return Optional.absent();
        }

        return optionalStationsPresent;
    }

    public void update(Collection<Station> newStations, long updatedTime) {
        final HashMap<String, Station> oldStations = Maps.newHashMap(stations);

        stations.clear();

        for (Station newStation : newStations) {
            final String id = newStation.getId();

            final Station oldStation = oldStations.get(id);
            final Station station = oldStation != null ? oldStation : new Station(id);

            station.update(newStation);

            stations.put(id, station);
        }

        this.updatedServerTime = updatedTime;

        for(OnStationListUpdateListener listener : listeners) {
            listener.onStationListUpdated();
        }
    }

    public void updateFromJson(JSONObject stationsInfoJsonObject) throws JSONException {
        final ArrayList<Station> newStations = new ArrayList<Station>();

        final JSONArray stationsJsonArray = stationsInfoJsonObject.getJSONArray("stations");
        final String updatedTimeString = stationsInfoJsonObject.getString("updatedTime");

        Date updatedTime;
        try {
            // Get the date in UTC
            updatedTime = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ", Locale.US).parse(updatedTimeString);
        } catch (ParseException e) {
            throw Throwables.propagate(e);
        }

        for (int i = 0; i < stationsJsonArray.length(); i++) {
            final JSONObject stationJsonObject = stationsJsonArray.getJSONObject(i);
            newStations.add(Station.fromJson(stationJsonObject));
        }

        update(newStations, updatedTime.getTime());
    }

    public Optional<Long> getUpdatedTimeIfLoaded() {
        if(stations != null) {
            return Optional.of(updatedServerTime);
        } else {
            return Optional.absent();
        }
    }

    public boolean takeUpdate() {
        final boolean wasUpdating = updating.getAndSet(true);

        if(!wasUpdating) {
            onUpdateStarted();
            return true;
        } else {
            return false;
        }
    }

    private void onUpdateStarted() {
        for (OnStationListUpdateListener listener : listeners) {
            listener.onStationListUpdateStarted();
        }
    }

    public void onUpdateFailure() {
        for (OnStationListUpdateListener listener : listeners) {
            listener.onStationListUpdateFailure();
        }
    }

    public void onUpdateCompleted() {
        updating.set(false);
        for (OnStationListUpdateListener listener : listeners) {
            listener.onStationListUpdateFinished();
        }
    }

    public boolean isUpdating() {
        return updating.get();
    }

    public void registerOnStationListUpdatedListener(OnStationListUpdateListener listener) {
        listeners.add(listener);
    }

    public  void unregisterOnStationListUpdatedListener(OnStationListUpdateListener listener) {
        listeners.remove(listener);
    }

    public interface OnStationListUpdateListener {
        void onStationListUpdateStarted();

        void onStationListUpdateFailure();

        void onStationListUpdateFinished();

        void onStationListUpdated();
    }
}