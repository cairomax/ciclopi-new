package com.example.ciclopi.model;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;

import com.google.common.base.Optional;
import com.google.common.base.Stopwatch;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class MyLocationInfo {
    private Location currentLocation;
    private double orientation = 0.0;

    private final List<MyLocationUpdateListener> listeners = new ArrayList<MyLocationUpdateListener>();

    public MyLocationInfo(Context context) {
        SensorManager sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        Sensor orientationSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);

        sensorManager.registerListener(new SensorEventListener() {
            Stopwatch stopwatch = new Stopwatch();

            {
                // TODO: move away
                stopwatch.start();
            }

            @Override
            public void onSensorChanged(final SensorEvent event) {
                if (stopwatch.elapsed(TimeUnit.MILLISECONDS) > 500) {
                    setOrientation(event);
                    stopwatch.reset();
                    stopwatch.start();
                }
            }

            @Override
            public void onAccuracyChanged(final Sensor sensor, final int accuracy) {

            }
        }, orientationSensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    public void setLocation(Location location) {
        currentLocation = location;
        fireUpdated();
    }

    public void setLocationUnknown() {
        if (currentLocation != null) {
            fireUpdated();
        }
        currentLocation = null;
    }

    public Optional<Location> getMyLocationIfAvailable() {
        return Optional.fromNullable(currentLocation);
    }

    public void setOrientation(SensorEvent event) {
        orientation = event.values[0];
        fireUpdated();
    }

    public double getOrientation() {
        return orientation;
    }

    private void fireUpdated() {
        for (MyLocationUpdateListener listener : listeners) {
            listener.onMyLocationUpdated();
        }
    }

    public void registerMyLocationUpdateListener(MyLocationUpdateListener listener) {
        listeners.add(listener);
    }

    public void unregisterMyLocationUpdateListener(MyLocationUpdateListener listener) {
        listeners.remove(listener);
    }

    public interface MyLocationUpdateListener {
        void onMyLocationUpdated();
    }
}
