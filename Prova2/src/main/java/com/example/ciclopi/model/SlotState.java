package com.example.ciclopi.model;

public enum SlotState {
    VACANT,
    BIKE,
    OUT_OF_SERVICE,
    UNKNOWN
}
