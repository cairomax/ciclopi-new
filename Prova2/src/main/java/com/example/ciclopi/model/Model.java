package com.example.ciclopi.model;

import android.content.Context;

public class Model {
    private final MyLocationInfo myLocationInfo;
    private final StationsInfo stationsInfo = new StationsInfo();
    private final ServerClock serverClock = new ServerClock();

    public Model(Context context) {
        myLocationInfo = new MyLocationInfo(context);
    }

    public StationsInfo getStationsInfo() {
        return stationsInfo;
    }

    public MyLocationInfo getMyLocationInfo() {
        return myLocationInfo;
    }

    public ServerClock getServerClock() {
        return serverClock;
    }
}
